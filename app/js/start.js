(function () {

  var body = document.querySelector('body');
  var main = document.querySelector('main');
  var pages = document.querySelectorAll('section');
  var header = document.querySelector('header');
  var links = document.getElementsByClassName('c-slider__nav-link');
  var slides = document.getElementsByClassName('c-slide');
  var backgrounds = document.getElementsByClassName('c-slider__background');
  var slideInterval;
  var linksCount = links.length;


  function addLoadedClass() {
    body.className += 'is-loaded';
    startSliding();
    objectFitImages();
  }

  [].forEach.call(document.getElementsByClassName('c-figcaption__expander'), function(expandCap) {

    expandCap.addEventListener('click', function(event) {

      if (expandCap.parentNode.classList.contains('is-active')) {
        expandCap.parentNode.classList.remove('is-active');
      } else {
        expandCap.parentNode.classList.add('is-active');
      }

    });
  });

  function startSliding() {
    slideInterval = setInterval (slideToNext, 5000);
  }

  function stopSliding() {
    clearInterval(slideInterval);
  }

  function findLinkIndex() {
    for(var i=0; i<linksCount; i++) {
      if(links[i].classList.contains('is-active')) {
        return i;
      }
    }
  }

  function slideToNext() {
    var linkIndex = findLinkIndex();
    var nextLinkIndex = (linkIndex + 1) % linksCount;

    selectSlide(links[nextLinkIndex]);
  }

  function selectSlide(navLink) {

    for (var i=0; i<links.length; i++) {
      links[i].classList.remove('is-active');
    }
    for (var i=0; i<backgrounds.length; i++) {
      backgrounds[i].classList.remove('is-active');
    }
    for (var i=0; i<slides.length; i++) {
      slides[i].classList.remove('is-active');
      slides[i].classList.remove('is-prev');
      slides[i].classList.remove('is-next');
    }
    var target = navLink.getAttribute('href');
    target = target.replace('#', '');
    var shape = target.split('-').slice(-1);
    var bg = shape + '-bg';
    var current = document.getElementById(target);
    var prev = current.previousElementSibling;
    var next = current.nextElementSibling;
    current.classList.add('is-active');
    navLink.classList.add('is-active');
    if(prev == null ) {
      var prev = document.querySelector('.c-slide--last');
    }
    if(next == null ) {
      var next = document.querySelector('.c-slide--first');
    }
    prev.classList.add('is-prev');
    next.classList.add('is-next');
    bg = document.getElementById(bg);
    bg.classList.add('is-active');
    if(shape == 'pacific') {
      morphPacific();
    }
    if(shape == 'mahal') {
      morphMahal();
    }
    if(shape == 'express') {
      morphExpress();
    }
    if(shape == 'lido') {
      morphLido();
    }
  }


  window.addEventListener('load', function(){

      setTimeout (addLoadedClass, 3000);

  }, false)

  document.body.addEventListener('touchstart', function(e){
      // alert('you touched me');
  }, false);

  // var timer = null;
  // document.addEventListener('wheel', function(e) {
  //   if(timer !== null) {
  //     clearTimeout(timer);
  //   }
  //   timer = setTimeout(function() {
  //     var acti = document.querySelector('.is-active');
  //     var next = document.querySelector('.is-next');
  //     var prev = document.querySelector('.is-prev');
  //     var prec = prev.previousElementSibling;
  //     var foll = next.nextElementSibling;
  //     if(prec == null ) {
  //       var prec = document.querySelector('.c-slide--last');
  //     }
  //     if(foll == null ) {
  //       var foll = document.querySelector('.c-slide--first');
  //     }
  //     var wDelta = e.wheelDelta < 0 ? 'up' : 'down';
  //     if(wDelta == 'down') {
  //       acti.classList.remove('is-active');
  //       acti.classList.add('is-prev');
  //       next.classList.add('is-active');
  //       next.classList.remove('is-next');
  //       foll.classList.add('is-next');
  //       prev.classList.remove('is-prev');
  //     }
  //     if(wDelta == 'up') {
  //       prev.classList.add('is-active');
  //       prev.classList.remove('is-prev');
  //       acti.classList.remove('is-active');
  //       acti.classList.add('is-next');
  //       next.classList.remove('is-next');
  //       prec.classList.add('is-prev');
  //     }
  //   }, 50);
  // });

  [].forEach.call(document.getElementsByClassName('c-slider__nav-link'), function(navLink) {

    navLink.addEventListener('click', function(event) {

      stopSliding();

      selectSlide(event.currentTarget);

    });
  });

}());
